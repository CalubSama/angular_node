const express = require('express');
const router = express.Router();
const mongoose = require ('mongoose');


const dbHost = "mongodb://database/angular-node";

mongoose.connect(dbHost);

const taskSchema = new mongoose.Schema({
    category: String,
    task: String,
    priority: Number,
    createAt: Date
});

const Task = mongoose.model('task', taskSchema);

router.get('/', (req, res)=>{
    res.send('El api funka bien')
})

router.get('/tasks', (req, res) => {
    Task.find({}, (err, tasks)=>{
        if(err) res.status(500).send(error)

        res.status(200).json(tasks);
    });
});

router.get('/tasks/:id', (req, res) => {
    Task.findById(req.param.id, (err, tasks)=>{
        if(err) res.status(500).send(error)

        res.status(200).json(tasks);
    });
});

router.post('/tasks/', (req, res) => {
    const newTask = new Task({
        category: req.body.category,
        task: req.body.task,
        priority: req.body.priority,
        createAt: Date.now()
    });
    newTask.save(error=>{
        if(error) res.status(500).send(error);

        res.status(201).json({
            message: 'Task created sucessfully'
        })
    })
});

router.delete('/tasks/:id', (req, res) => {
    Task.delete(req.param.id, (err, tasks)=>{
        if(err) res.status(500).send(error)

        res.status(200).json({
            message: 'Task deleted sucessfully'
        });
    })
});



module.exports = router;